# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/12/07 01:27:33 by tiboitel          #+#    #+#              #
#    Updated: 2015/05/10 20:39:54 by tiboitel         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME 		 =	fractol
SRCS 		 = 	main.c \
				core_loop.c \
				graphic_engine.c \
				mdl_fractol.c \
				mgr_thread.c \
				mandelbrot.c \
				cubemandelbrot.c \
				birdofprey.c \
				burningship.c \
				julia.c \
				ft_img.c \
				ft_img2.c \
				divide_square.c \
				hook.c
INCLUDES	 =	./includes
SRCSPATH 	 =	./srcs/
LIBFTPATH 	 =  ./libft/
LIBFTINC 	 =  ./libft/includes

CC			 = gcc
CFLAGS		 = -Wall -Werror -Wextra -Ofast
INCLUDES_O	 = -I $(LIBFTINC) -I $(INCLUDES)
INCLUDES_C	 = -L $(LIBFTPATH) -lft -L/usr/lib -lmlx -framework OpenGL -framework AppKit -lm

SRC			 = $(addprefix $(SRCSPATH), $(SRCS))
OBJS		 = $(SRC:.c=.o)

all:		$(NAME)

$(NAME):	$(OBJS)
			$(CC) -o $(NAME) $(OBJS) $(CFLAGS) $(INCLUDES_C)

%.o: %.c libft/libft.a
			$(CC) -o $@ $(CFLAGS) $(INCLUDES_O) -c $<

libft/libft.a:
			make -C $(LIBFTPATH)

clean:
			make -C $(LIBFTPATH) clean
			rm -rf $(OBJS)

fclean: 	clean
			make -C $(LIBFTPATH) fclean
			rm -rf $(NAME)

re: fclean all

.PHONY: clean fclean re
			
