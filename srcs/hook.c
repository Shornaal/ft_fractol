/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/06 14:33:20 by tiboitel          #+#    #+#             */
/*   Updated: 2015/05/10 21:14:15 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

static int	key_hook2(int keycode, t_mgr_fractol *frctl)
{
	if (keycode == 78)
	{
		frctl->frctl->curr_frctl -= (frctl->frctl->curr_frctl > 0) ? 1 : 0;
		frctl->frctl->zoom = 1;
	}
	else if (keycode == 69)
	{
		frctl->frctl->zoom = 1;
		frctl->frctl->curr_frctl += (frctl->frctl->curr_frctl < 4) ? 1 : 0;
	}
	else if (keycode == 88)
		frctl->frctl->base_iter += 10;
	else if (keycode == 92)
		frctl->frctl->base_iter -= 10;
	return (0);
}

int			key_hook(int keycode, t_mgr_fractol *frctl)
{
	if (keycode == 53)
		exit(0);
	else if (keycode == 12)
		frctl->is_anim = (frctl->is_anim) ? 0 : 1;
	else if (keycode == 123)
		frctl->frctl->movex -= 0.1 / frctl->frctl->zoom;
	else if (keycode == 124)
		frctl->frctl->movex += 0.1 / frctl->frctl->zoom;
	else if (keycode == 126)
		frctl->frctl->movey -= 0.1 / frctl->frctl->zoom;
	else if (keycode == 125)
		frctl->frctl->movey += 0.1 / frctl->frctl->zoom;
	else
		key_hook2(keycode, frctl);
	return (0);
}

int			mouse_motion(int x, int y, t_mgr_fractol *frctl)
{
	x = x - (WINDW_W / 2);
	y = y - (WINDW_H / 2);
	frctl->frctl->cre = (double)x / 256 * frctl->frctl->zoom;
	frctl->frctl->cim = (double)y / 256 * frctl->frctl->zoom;
	return (0);
}

int			mouse_hook(int button, int x, int y, t_mgr_fractol *frctl)
{
	(void)x;
	(void)y;
	if (button == 4)
	{
		frctl->frctl->zoom *= 1.1;
		if (((int)frctl->frctl->zoom % 12 == 0) &&
				frctl->frctl->base_iter < 512)
			frctl->frctl->base_iter *= (frctl->frctl->curr_frctl == 1)
				? 1.4 : 1.1;
	}
	else if (button == 5)
	{
		frctl->frctl->zoom /= 1.1;
		if (((int)frctl->frctl->zoom % 12 == 0) &&
				frctl->frctl->base_iter < 512)
			frctl->frctl->base_iter /= (frctl->frctl->curr_frctl == 1)
				? 1.4 : 1.1;
	}
	return (0);
}
