/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   divide_square.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 20:38:28 by tiboitel          #+#    #+#             */
/*   Updated: 2015/05/10 20:39:24 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

void				divide_square2(t_mdl_fractol *frctl, int i)
{
	if (i == 2)
	{
		frctl->x1 = 0;
		frctl->x2 = WINDW_W / 2;
		frctl->y1 = WINDW_H / 2;
		frctl->y2 = WINDW_H;
	}
	else if (i == 3)
	{
		frctl->x1 = WINDW_W / 2;
		frctl->x2 = WINDW_W;
		frctl->y1 = WINDW_H / 2;
		frctl->y2 = WINDW_H;
	}
}

t_mdl_fractol		*divide_square(t_mdl_fractol *frctl, int i)
{
	if (i == 0)
	{
		frctl->x1 = 0;
		frctl->x2 = WINDW_W / 2;
		frctl->y1 = 0;
		frctl->y2 = WINDW_H / 2;
	}
	else if (i == 1)
	{
		frctl->x1 = WINDW_W / 2;
		frctl->x2 = WINDW_W;
		frctl->y1 = 0;
		frctl->y2 = WINDW_H / 2;
	}
	else
		divide_square2(frctl, i);
	return (frctl);
}
