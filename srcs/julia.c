/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/08 19:04:01 by tiboitel          #+#    #+#             */
/*   Updated: 2015/05/10 20:48:38 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

void		julia(t_mdl_fractol *e)
{
	int			x;
	int			y;
	int			i;
	int			color;

	x = e->x1;
	while (y = e->y1, x++ < e->x2)
		while (i = 0, y++ < e->y2)
		{
			e->newre = 1.5 * (x - WINDW_W / 2) /
				(0.5 * e->zoom * WINDW_W) + e->movex;
			e->newim = (y - WINDW_H / 2) / (0.5 * e->zoom * WINDW_H)
				+ e->movey;
			while (i++ < (e->base_iter + e->max_iter))
			{
				e->oldre = e->newre;
				e->oldim = e->newim;
				e->newre = MPOW(e->oldre) - MPOW(e->oldim) + e->cre;
				e->newim = 2 * e->oldre * e->oldim + e->cim;
				if ((MPOW(e->newre) + MPOW(e->newim)) > 4)
					break ;
			}
			color = (0xA6 * i) + (0x42 + i * i) + (0x58 * i);
			ft_pixel_put_to_image(e->img, x, y, color);
		}
}
