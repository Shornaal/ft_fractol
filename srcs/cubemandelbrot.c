/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cubemandelbrot.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 20:45:43 by tiboitel          #+#    #+#             */
/*   Updated: 2015/05/10 20:46:15 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

static void		cube_doop(int x, int y, t_mdl_fractol *frctl)
{
	frctl->cre = 2 * (x - WINDW_W / 2) / (0.5 * frctl->zoom * WINDW_W)
		+ frctl->movex;
	frctl->cim = 2 * (y - (WINDW_H / 2)) /
		(0.5 * frctl->zoom * WINDW_H) + frctl->movey;
	frctl->newre = 0;
	frctl->newim = 0;
	frctl->oldre = 0;
	frctl->oldim = 0;
}

int				cubemandelbrot(t_mdl_fractol *e)
{
	int				x;
	int				y;
	int				i;
	int				color;

	x = e->x1;
	while (y = e->y1, x++ < e->x2)
		while (i = 0, y++ < e->y2)
		{
			cube_doop(x, y, e);
			while (i++ < (e->base_iter + e->max_iter))
			{
				e->oldre = e->newre;
				e->oldim = e->newim;
				e->newre = CUBE(e->oldre) - (3 * e->oldre * MPOW(e->oldim)) +
					e->cre;
				e->newim = (3 * MPOW(e->oldre) * e->oldim) - CUBE(e->oldim)
					+ e->cim;
				if (MPOW(e->newre) + MPOW(e->newim) > 4)
					break ;
			}
			color = (i % 0xA6) * (0x13 * i / 50) * 0x59 * (i < (e->base_iter));
			ft_pixel_put_to_image(e->img, x, y, color);
		}
	return (0);
}
