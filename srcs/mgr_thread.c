/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mgr_thread.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/30 20:48:42 by tiboitel          #+#    #+#             */
/*   Updated: 2015/05/10 20:38:43 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>
#include <stdio.h>

t_mgr_thread		*thrd_create(void)
{
	t_mgr_thread	*mgr_thrd;

	if (!(mgr_thrd = (t_mgr_thread *)ft_memalloc(sizeof(t_mgr_thread))))
		return (NULL);
	return (mgr_thrd);
}

void				thrd_init(t_mgr_thread *mgr_thrd)
{
	mgr_thrd->mutex_fractol =
		(pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
}

void				*thrd_frctl(void *data)
{
	if (((t_mdl_fractol *)(data))->curr_frctl == 0)
		mandelbrot(((t_mdl_fractol *)(data)));
	else if (((t_mdl_fractol *)(data))->curr_frctl == 1)
		julia((t_mdl_fractol *)(data));
	else if (((t_mdl_fractol *)(data))->curr_frctl == 2)
		burningship(((t_mdl_fractol *)(data)));
	else if (((t_mdl_fractol *)(data))->curr_frctl == 3)
		cubemandelbrot(((t_mdl_fractol *)(data)));
	else if (((t_mdl_fractol *)(data))->curr_frctl == 4)
		birdofprey(((t_mdl_fractol *)(data)));
	return (NULL);
}

int					thrd_launch(t_mgr_fractol *mgr_frctl)
{
	int				i;
	t_mdl_fractol	*buffer[NBR_THREADS];

	pthread_mutex_lock(&(mgr_frctl->mgr_thrd->mutex_fractol));
	i = -1;
	while (++i < NBR_THREADS)
	{
		buffer[i] = NULL;
		if (!(buffer[i] = (t_mdl_fractol *)ft_memalloc(sizeof(t_mdl_fractol))))
			return (-1);
		ft_memcpy(buffer[i], mgr_frctl->frctl, sizeof(t_mdl_fractol));
		divide_square(buffer[i], i);
		pthread_create(&(mgr_frctl->mgr_thrd->threads[i]), NULL, thrd_frctl,
				buffer[i]);
	}
	i = -1;
	while (++i < NBR_THREADS)
	{
		pthread_join(mgr_frctl->mgr_thrd->threads[i], NULL);
		ft_memcpy(mgr_frctl->frctl->img, buffer[i]->img, sizeof(t_img));
		free(buffer[i]);
	}
	pthread_mutex_unlock(&(mgr_frctl->mgr_thrd->mutex_fractol));
	return (1);
}

void				thrd_destroy(t_mgr_thread *mgr_thrd)
{
	free(mgr_thrd);
}
