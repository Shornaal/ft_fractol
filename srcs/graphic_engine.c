/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   graphic_engine.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/29 11:33:45 by tiboitel          #+#    #+#             */
/*   Updated: 2015/05/10 21:15:24 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

t_mgr_fractol		*gr_create(void)
{
	t_mgr_fractol	*mgr_fractol;

	if (!(mgr_fractol = (t_mgr_fractol *)ft_memalloc(sizeof(t_mgr_fractol))))
		return (NULL);
	mgr_fractol->mgr_thrd = NULL;
	mgr_fractol->frctl = NULL;
	return (mgr_fractol);
}

int					gr_init(t_mgr_fractol *mgr_fractol)
{
	mgr_fractol->mlx = mlx_init();
	mgr_fractol->windw = mlx_new_window(mgr_fractol->mlx, WINDW_W, WINDW_H,
		"fractol");
	mgr_fractol->is_anim = 0;
	if (!(mgr_fractol->frctl = mdl_fcreate()))
		return (-1);
	mdl_finit(mgr_fractol->frctl);
	if (!(mgr_fractol->mgr_thrd = thrd_create()))
		return (-1);
	mgr_fractol->frctl->img = ft_new_img(mgr_fractol->mlx, WINDW_W, WINDW_H);
	thrd_init(mgr_fractol->mgr_thrd);
	return (1);
}

int					gr_render(t_mgr_fractol *mgr_fractol)
{
	if (thrd_launch(mgr_fractol))
		mlx_put_image_to_window(mgr_fractol->mlx, mgr_fractol->windw,
	mgr_fractol->frctl->img->img,
	-1, 0);
	return (0);
}
