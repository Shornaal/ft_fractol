/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/29 19:06:40 by tiboitel          #+#    #+#             */
/*   Updated: 2015/05/10 20:44:48 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

static void		mandel_doop(int x, int y, t_mdl_fractol *frctl)
{
	frctl->cre = 2 * (x - WINDW_W / 2) / (0.5 * frctl->zoom * WINDW_W)
		+ frctl->movex;
	frctl->cim = 2 * (y - (WINDW_H / 2)) /
		(0.5 * frctl->zoom * WINDW_H) + frctl->movey;
	frctl->newre = 0;
	frctl->newim = 0;
	frctl->oldre = 0;
}

int				mandelbrot(t_mdl_fractol *frctl)
{
	int				x;
	int				y;
	int				i;
	int				color;

	x = frctl->x1;
	while (y = frctl->y1, x++ < frctl->x2)
		while (i = 0, y++ < frctl->y2)
		{
			mandel_doop(x, y, frctl);
			while (i++ < (frctl->base_iter + frctl->max_iter))
			{
				frctl->oldre = frctl->newre;
				frctl->oldim = frctl->newim;
				frctl->newre = MPOW(frctl->oldre) - MPOW(frctl->oldim) +
					frctl->cre;
				frctl->newim = 2 * frctl->oldre * frctl->oldim + frctl->cim;
				if (MPOW(frctl->newre) + MPOW(frctl->newim) > 4)
					break ;
			}
			color = (i % 0xA6) * (0x13 * i / 50) * 0x59 *
				(i < (frctl->base_iter + frctl->max_iter));
			ft_pixel_put_to_image(frctl->img, x, y, color);
		}
	return (0);
}
