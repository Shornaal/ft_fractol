/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   core_loop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/29 11:34:31 by tiboitel          #+#    #+#             */
/*   Updated: 2015/05/10 20:54:28 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>
#include <stdio.h>

int		run(t_mgr_fractol *mgr_fractol)
{
	static long	curr_time;
	static int	sign;

	curr_time = (long)time(NULL);
	if (mgr_fractol->is_anim)
	{
		if ((curr_time % 3) == 0)
			sign = 1;
		else if (mgr_fractol->frctl->max_iter <= 1)
			sign = 0;
		if (sign == 1)
			mgr_fractol->frctl->max_iter -= 1 + (curr_time % 3);
		else
			mgr_fractol->frctl->max_iter += (curr_time % 3);
	}
	gr_render(mgr_fractol);
	return (1);
}

int		expose_hook(t_mgr_fractol *mgr_fractol)
{
	run(mgr_fractol);
	return (0);
}

int		cr_init(int curr_frctl)
{
	t_mgr_fractol	*mgr_fractol;

	if (!(mgr_fractol = gr_create()))
		return (0);
	gr_init(mgr_fractol);
	mgr_fractol->frctl->curr_frctl = curr_frctl;
	if (mgr_fractol->frctl->curr_frctl == 1)
	{
		mgr_fractol->frctl->cre = -0.7;
		mgr_fractol->frctl->cim = 0.27015;
	}
	mlx_hook(mgr_fractol->windw, 6, (1L << 6), mouse_motion, mgr_fractol);
	mlx_key_hook(mgr_fractol->windw, key_hook, mgr_fractol);
	mlx_mouse_hook(mgr_fractol->windw, mouse_hook, mgr_fractol);
	mlx_expose_hook(mgr_fractol->windw, expose_hook, mgr_fractol);
	mlx_loop_hook(mgr_fractol->mlx, expose_hook, mgr_fractol);
	mlx_loop(mgr_fractol->mlx);
	return (1);
}

int		cr_update(void)
{
	return (0);
}
