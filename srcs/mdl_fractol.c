/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mdl_fractol.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/30 20:20:14 by tiboitel          #+#    #+#             */
/*   Updated: 2015/05/10 20:43:39 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

t_mdl_fractol	*mdl_fcreate(void)
{
	t_mdl_fractol	*mdl_frctl;

	if (!(mdl_frctl = (t_mdl_fractol *)ft_memalloc(sizeof(t_mdl_fractol))))
		return (NULL);
	ft_bzero(mdl_frctl, sizeof(t_mdl_fractol));
	return (mdl_frctl);
}

void			mdl_finit(t_mdl_fractol *mdl_frctl)
{
	mdl_frctl->cre = 0;
	mdl_frctl->cim = 0;
	mdl_frctl->newre = 0;
	mdl_frctl->newim = 0;
	mdl_frctl->oldre = 0;
	mdl_frctl->oldim = 0;
	mdl_frctl->zoom = 1;
	mdl_frctl->movex = 0;
	mdl_frctl->movey = 0;
	mdl_frctl->base_iter = 48;
	mdl_frctl->max_iter = 0;
	mdl_frctl->x1 = 0;
	mdl_frctl->x2 = 0;
	mdl_frctl->y1 = 0;
	mdl_frctl->y2 = 0;
}

void			mdl_fdestroy(t_mdl_fractol *mdl_frctl)
{
	free(mdl_frctl);
}
