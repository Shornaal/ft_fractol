/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_img.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/06 16:08:44 by tiboitel          #+#    #+#             */
/*   Updated: 2015/05/07 17:35:39 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

void	ft_fill_img(t_img *i, unsigned int color)
{
	int		x;
	int		y;

	x = -1;
	while ((y = -1), ++x < i->width)
		while (++y < i->height)
		{
			i->udata[y * i->sl_div + x * i->bpp_div] = color;
		}
}

t_uint	ft_get_pixel_from_image(t_img *i, int x, int y)
{
	unsigned int color;

	if (!i)
		return (0);
	if (x > i->width || x < 0 || y < 0 || y > i->height)
		return (0);
	color = i->udata[y * i->sl_div + x * i->bpp_div];
	return (color);
}

void	ft_pixel_put_to_image(t_img *i, int x, int y, unsigned int color)
{
	if (!i)
		return ;
	if (x > i->width || x < 0 || y < 0 || y > i->height)
		return ;
	i->udata[y * i->sl_div + x * i->bpp_div] = color;
}

t_img	*ft_load_img(void *mlx, char *src)
{
	t_img	*i;

	if (!src || !(i = (t_img*)ft_memalloc(sizeof(t_img))))
		return (NULL);
	if (!(i->img = mlx_xpm_file_to_image(mlx, src, &(i->width), &(i->height))))
		return (NULL);
	i->data = mlx_get_data_addr(i->img, &(i->bpp),
								&(i->sizeline), &(i->endian));
	i->sl_div = i->sizeline / 4;
	i->bpp_div = i->bpp / 8 / 4;
	i->udata = (unsigned int*)i->data;
	return (i);
}

t_img	*ft_new_img(void *mlx, int width, int height)
{
	t_img	*i;

	if (!(i = (t_img*)ft_memalloc(sizeof(t_img))))
		return (NULL);
	i->width = width;
	i->height = height;
	i->img = mlx_new_image(mlx, width, height);
	i->data = mlx_get_data_addr(i->img, &(i->bpp),
								&(i->sizeline), &(i->endian));
	i->sl_div = i->sizeline / 4;
	i->bpp_div = i->bpp / 8 / 4;
	i->udata = (unsigned int*)i->data;
	return (i);
}
