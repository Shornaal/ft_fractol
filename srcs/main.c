/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/16 21:12:30 by tiboitel          #+#    #+#             */
/*   Updated: 2015/05/10 20:35:04 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>
#include <execinfo.h>
#include <signal.h>
#include <stdio.h>

static void		ft_usage(void)
{
	ft_putendl("Usage: write args: ");
	ft_putendl("- Mandelbrot.");
	ft_putendl("- Julia.");
	ft_putendl("- Burningship.");
	ft_putendl("- Cubemandelbrot.");
	ft_putendl("- Birdofprey.");
}

static int		curr_choive(char **argv)
{
	int		curr_frctl;

	curr_frctl = 0;
	argv[1] = ft_strtolower(argv[1]);
	if (!ft_strcmp(argv[1], "mandelbrot"))
		curr_frctl = 0;
	else if (!ft_strcmp(argv[1], "julia"))
		curr_frctl = 1;
	else if (!ft_strcmp(argv[1], "burningship"))
		curr_frctl = 2;
	else if (!ft_strcmp(argv[1], "cubemandelbrot"))
		curr_frctl = 3;
	else if (!ft_strcmp(argv[1], "birdofprey"))
		curr_frctl = 4;
	else
	{
		ft_usage();
		exit(-1);
	}
	return (curr_frctl);
}

int				main(int argc, char **argv, char **env)
{
	if (argc != 2 || !*env || !env)
	{
		ft_usage();
		return (-1);
	}
	cr_init(curr_choive(argv));
	return (0);
}
