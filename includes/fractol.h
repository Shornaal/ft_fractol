/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/16 21:15:13 by tiboitel          #+#    #+#             */
/*   Updated: 2015/05/10 20:43:59 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_FRACTOL_H
# define FT_FRACTOL_H
# include <libft.h>
# include <mlx.h>
# include <math.h>
# include <time.h>
# include <pthread.h>
# include <ft_mlx.h>
# include <stdlib.h>
# define DESIRED_FRAME 60
# define ABS(x) (((x) < 0) ? -(x) : (x))
# define CUBE(x) (x * x * x)
# define MPOW(x) (x * x)
# define WINDW_W 2560 / 2
# define WINDW_H 1440 / 2
# define NBR_THREADS 4

typedef struct		s_mgr_thread
{
	pthread_t		threads[NBR_THREADS];
	pthread_mutex_t	mutex_fractol;
}					t_mgr_thread;

typedef struct		s_mdl_fractol
{
	double			cre;
	double			cim;
	double			newre;
	double			newim;
	double			oldre;
	double			oldim;
	double			zoom;
	double			movex;
	double			movey;
	int				base_iter;
	int				max_iter;
	int				x1;
	int				x2;
	int				y1;
	int				y2;
	t_img			*img;
	int				curr_frctl;
}					t_mdl_fractol;

typedef struct		s_mgr_fractol
{
	void			*mlx;
	void			*windw;
	t_mgr_thread	*mgr_thrd;
	t_mdl_fractol	*frctl;
	int				is_anim;
}					t_mgr_fractol;

int					run(t_mgr_fractol	*mgr_fractol);
int					cr_init(int curr_frctl);
int					cr_update();
t_mgr_fractol		*gr_create();
int					key_hook(int keycode, t_mgr_fractol *frctl);
int					mouse_hook(int button, int x, int y, t_mgr_fractol *frctl);
int					mouse_motion(int x, int y, t_mgr_fractol *frctl);
int					gr_render(t_mgr_fractol *mgr_fractol);
int					gr_init(t_mgr_fractol *mgr_fractol);
t_mdl_fractol		*mdl_fcreate();
void				mdl_finit(t_mdl_fractol *mdl_frctl);
void				mdl_fdestroy(t_mdl_fractol *mdl_frctl);
t_mgr_thread		*thrd_create();
void				thrd_init(t_mgr_thread *mgr_thrd);
void				thrd_destroy(t_mgr_thread *mgr_thrd);
void				*thrd_frctl(void *data);
int					thrd_launch(t_mgr_fractol *mgr_frctl);
int					mandelbrot(t_mdl_fractol *frctl);
int					birdofprey(t_mdl_fractol *frctl);
int					cubemandelbrot(t_mdl_fractol *frctl);
void				burningship(t_mdl_fractol *frctl);
void				julia(t_mdl_fractol *frctl);
t_mdl_fractol		*divide_square(t_mdl_fractol *frctl, int i);
#endif
